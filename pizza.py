from flask import Flask, render_template, request, redirect, url_for, jsonify

app = Flask("iPizza")

class Order:
	def __init__(self, person, toppings):
		self.person = person
		self.toppings = toppings

data = [
			Order("Tom", "szynka, pieczarki"), 
			Order("Jerry", "Cztery Sery")
]

@app.route("/<day>/")
def index(day):
    return render_template("index.html", day=day, data=data)


@app.route("/<day>/add", methods=["POST"])
def add(day):
    data.append(Order(request.form['person'], request.form['toppings']))
    return redirect(url_for('index', day=day))


@app.route("/api/", methods=["GET", "POST"])
def api_index():
	if request.method == "POST":
		order = Order(request.json['person'], request.json['toppings'])
		data.append(order)
		return jsonify(index=data.index(order), **order.__dict__)
	else:
		return jsonify(objects=[order.__dict__ for order in data])

if __name__ == "__main__":
	app.run(debug=True)