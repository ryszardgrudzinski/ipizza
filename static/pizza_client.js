angular.module('pizzaClient', [])
  .controller('OrderListController', function($scope, $http) {
    $scope.data = [];
    $scope.form_data = {};

    $scope.add = function() {
      $http.post("/api/", $scope.form_data).then(function(res) {
        $scope.data.push(res.data);
        $scope.form_data = {};
      })
    }

    $http.get('/api/').then(function(res) {
      $scope.data = res.data.objects;
    });
  });